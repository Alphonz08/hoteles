import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ApiService } from '../api.service';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-reservacion',
  templateUrl: './reservacion.component.html',
  styleUrls: ['./reservacion.component.css']
})
export class ReservacionComponent implements OnInit {
  name = new FormControl('');
  description = new FormControl('');
  email = new FormControl('');

  constructor(private api : ApiService, private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  guardar(){
    let data = {
      name: this.name.value,
      description: this.description.value,
      email: this.email.value
    };
    this.api.create(data).subscribe();
    this.activeModal.close();
  }
}
