import { Component } from '@angular/core';
import { ApiService } from './api.service';
import { FormControl } from '@angular/forms';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReservacionComponent } from './reservacion/reservacion.component';
import { EditarComponent } from './editar/editar.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'hoteles';
  id = new FormControl('');

  reservaciones: any = [];
  constructor(private api: ApiService, private modalService: NgbModal) {
    this.api.getAll().subscribe(response => {
      this.reservaciones = response;
    });
  }

  open() {
    this.modalService.open(ReservacionComponent);
  }

  openedit(id) {
    const modalRef = this.modalService.open(EditarComponent);
    modalRef.componentInstance.id = id;
  }
  eliminar(id) {
    this.api.delete(id).subscribe();
  }

  buscar() {
    this.api.getAllById(this.id.value).subscribe(response => {
      this.reservaciones = response;
    });
  }
}
