import { Component, OnInit,Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ApiService } from '../api.service';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
  name = new FormControl('');
  description = new FormControl('');
  email = new FormControl('');
  @Input() id;

  reserva: any [];

  constructor(private api : ApiService, private activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.api.getAllById(this.id).subscribe(response => {
      this.reserva = response;
      this.name.setValue(response[0].name);
      this.description.setValue(response[0].description);
      this.email.setValue(response[0].email);
    });
  }

  editar(){
    let data = {
      name: this.name.value,
      description: this.description.value,
      email: this.email.value
    };
    this.api.update(this.id,data).subscribe();
    this.activeModal.close();
  }

}
