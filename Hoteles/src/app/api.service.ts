import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  api: string = 'http://127.0.0.1:8000/api/reservacion';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any>{
    return this.http.get(this.api);
  }

  getAllById(id: string): Observable<any>{
    return this.http.get(`${this.api}/${id}`);
  }

  create(data: any): Observable<any>{
    return this.http.post(`${this.api}`,data);
  }

  update(id: string,data: any): Observable<any>{
    return this.http.put(`${this.api}/${id}`,data)
  }

  delete(id: string): Observable<any>{
    return this.http.delete(`${this.api}/${id}`);
  }
}
