import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservacionComponent } from './reservacion/reservacion.component';
import { AppComponent } from './app.component';
import { EditarComponent } from './editar/editar.component';

const routes: Routes = [
  {
    path: 'crear',
    component: ReservacionComponent
  },
  {
    path: 'editar',
    component: EditarComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
