<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/reservacion','ReservacionController@index');
Route::get('/reservacion/{id}','ReservacionController@show');
Route::post('/reservacion','ReservacionController@store');
Route::put('/reservacion/{id}','ReservacionController@update');
Route::delete('/reservacion/{id}','ReservacionController@destroy');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
