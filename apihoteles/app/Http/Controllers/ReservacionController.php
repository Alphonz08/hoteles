<?php

namespace App\Http\Controllers;

use App\Reservacion;
use Illuminate\Http\Request;

class ReservacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservacion = Reservacion::all();
        return $reservacion;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reservacion = new Reservacion;
            $reservacion->name = $request->input('name');
            $reservacion->description = $request->input('description');
            $reservacion->email = $request->input('email');
            $reservacion->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reservacion = Reservacion::where('id', $id)->get();
        return $reservacion;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reservacion = Reservacion::find($id);
            $reservacion->name = $request->input('name');
            $reservacion->description = $request->input('description');
            $reservacion->email = $request->input('email');
            $reservacion->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservacion = Reservacion::find($id);
        $reservacion->delete();
    }
}
