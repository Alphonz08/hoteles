<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservacion extends Model
{
    protected $table = 'reservacion';

    protected $fillable = [
        'name', 'description', 'email',
    ];

    protected $visible = [
        'id', 'name', 'description', 'email'
    ];
}
